// `CheckerPlugin` is optional. Use it if you want async error reporting.
// We need this plugin to detect a `--watch` mode. It may be removed later
// after https://github.com/webpack/webpack/issues/3460 will be resolved.
const path = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
var HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  context: path.resolve('.', "src"),
  entry: './main.ts',
  // Currently we need to add '.ts' to the resolve.extensions array.
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  output: {
    path: path.resolve('.', 'dist'),
    publicPath: "/",
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].map'
  },
  // Source maps support ('inline-source-map' also works)
  devtool: 'cheap-module-source-map',

  // Add the loader for .ts files.
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      }
    ]
  },
  plugins: [
    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      title: 'Timeline',
      template: 'index.ejs'
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000,
    open: true
  }
};
