declare const Chart: any;

export function registerDailyScale() {
  const helpers = Chart.helpers;
  const isArray = helpers.isArray;

  class DailyScale extends Chart.scaleService.getScaleConstructor('linear') {
    public buildTicks() {
      let opts = this.options;
      let tickOpts = opts.ticks;
      tickOpts.min = 0;
      tickOpts.max = 24;

      // Figure out what the max number of ticks we can support it is based on the size of
      // the axis area. For now, we say that the minimum tick spacing in pixels must be 50
      // We also limit the maximum number of ticks to 11 which gives a nice 10 squares on
      // the graph. Make sure we always have at least 2 ticks
      let maxTicks = this.getTickLimit();
      maxTicks = Math.max(2, maxTicks);

      const numericGeneratorOptions = {
        maxTicks: maxTicks,
        min: tickOpts.min,
        max: tickOpts.max,
        stepSize: 1
      };
      let ticks = this.ticks = Chart.Ticks.generators.linear(numericGeneratorOptions, this);

      this.handleDirectionalChanges();

      // At this point, we need to update our max and min given the tick values since we have expanded the
      // range of the scale
      this.max = helpers.max(ticks);
      this.min = helpers.min(ticks);

      if (tickOpts.reverse) {
        ticks.reverse();
        this.start = this.max;
        this.end = this.min;
      } else {
        this.start = this.min;
        this.end = this.max;
      }
    }
  }

  Chart.scaleService.registerScaleType('daily', DailyScale);
}
