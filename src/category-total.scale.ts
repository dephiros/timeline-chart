declare const Chart: any;

export function registerCategoryTotalScale() {
  const helpers = Chart.helpers;
  const globalDefaults = Chart.defaults.global;

  class CategoryTotalScale extends Chart.scaleService.getScaleConstructor('category') {
    public draw(chartArea) {
      super.draw(chartArea);
      const context = this.ctx;
      const tickOpts = this.options.ticks;
      // tick len + padding
      const tickLen = (this.options.gridLines.drawTicks ? this.options.gridLines.tickMarkLength : 0);
      const tickOffset = tickLen + tickOpts.padding;
      context.save();
      context.beginPath();
      context.moveTo(this.left + tickOffset, this.bottom);
      context.lineTo(this.right, this.bottom);
      context.stroke();
      context.textBaseline = 'top';
      context.font = this.getFontTextFromOptions(tickOpts);
      context.fillText(
        // empty callback to get total
        (tickOpts.totalCallback ? tickOpts.totalCallback(this.ticks) : ""),
        this.left + tickOffset,
        this.bottom + 5);
      context.restore();
    }

    private getFontTextFromOptions(options) {
      const getValueOrDefault = helpers.getValueOrDefault;
      return helpers.fontString(
        getValueOrDefault(options.fontSize, globalDefaults.defaultFontSize),
        getValueOrDefault(options.fontStyle, globalDefaults.defaultFontStyle),
        getValueOrDefault(options.fontFamily, globalDefaults.defaultFontFamily)
      );
    }
  }

  Chart.scaleService.registerScaleType('categoryTotal', CategoryTotalScale);
}
