declare const Chart: any;

export function registerTimeline() {
  const helpers = Chart.helpers;
  const globalDefaults = Chart.defaults.global;
  const isArray = helpers.isArray;
  const DST_BG = '#2d2d2d';
  const DST_TEXT_COLOR = '#ffffff';
  const DST_PLUS_TEXT = '+1';
  const DST_MINUS_TEXT = '-1';
  const YPADDING = 10;
  let barHeight; // slightly hacky way to keep track of current bar height

  Chart.controllers.timeline = Chart.DatasetController.extend({
    dataElementType: Chart.elements.Rectangle,

    update: function (reset) {
      const me = this;
      const meta = me.getMeta();
      helpers.each(meta.data, function (box, index) {
        me.updateElement(box, index, reset);
      }, me);
    },

    updateElement: function (box, index, reset) {
      const me = this;
      const meta = me.getMeta();
      const xScale = me.getScaleForId(meta.xAxisID);
      const yScale = me.getScaleForId(meta.yAxisID);
      const dataset = me.getDataset();
      const data = dataset.data[index];
      const datasetIndex = me.index;

      box._xScale = xScale;
      box._yScale = yScale;
      box._datasetIndex = me.index;
      box._index = index;

      const ruler = me.getRuler(index);
      const base = xScale.getPixelForValue(
        Math.min(Math.max(data.start, xScale.start), xScale.end),
        index,
        datasetIndex);
      const x = xScale.getPixelForValue(
        Math.min(Math.max(data.end, xScale.start), xScale.end),
        index,
        datasetIndex);
      const height = barHeight = ruler.tickHeight - YPADDING;
      const y = yScale.getPixelForValue(data, datasetIndex, datasetIndex)
        + YPADDING / 2 + height / 2; // get the midpoint
      const color = me.chart.options.fillFunction(data);

      const chartMid = (this.chart.chartArea.bottom - this.chart.chartArea.top) / 2;

      // model should not contain width. Otherwise tooltip code will think that this is vertical bar
      // this will get processed to _view
      box._model = {
        data: data,
        x: (reset ? base : x),
        y: y,
        height: height,
        base: base,
        backgroundColor: (reset ? "#FFFF" : color),
        // Tooltip
        label: me.chart.data.labels[datasetIndex],
        datasetLabel: dataset.label,
        // callout
        callOutX: (reset ? base : x),
        callOutY: y
      };

      // this override the draw function of rectangle element
      box.draw = function () {
        const ctx = this._chart.ctx;
        const vm = this._view;
        // this is due to DST. If event start at 01:04 PDT and end at 01:00 PST (1 hour and 4 min duration)
        if (vm.base > vm.x) { return; }
        ctx.fillStyle = vm.backgroundColor;
        ctx.strokeStyle = me.chart.options.borderColor;
        ctx.lineWidth = me.chart.options.borderWidth;
        helpers.drawRoundedRectangle(
          ctx,
          vm.base,
          vm.y - vm.height / 2,
          vm.x - vm.base,
          vm.height,
          2
        );
        ctx.fill();
        ctx.stroke();
      };
      // additional function to help draw callout
      box.drawCallOut = function () {
        const ctx = this._chart.ctx;
        const vm = this._view;
        // this is due to DST. If event start at 01:04 PDT and end at 01:00 PST (1 hour and 4 min duration)
        if (vm.base > vm.x) { return; }
        if (data.callOut) {
          me.drawCallOutHelper(
            ctx, data.callOut,
            vm.callOutX,
            vm.callOutY,
            me.chart.options.callOut, {
              bottomLeft: y < chartMid,
              topLeft: y >= chartMid
            });
        }
      };

      box.tooltipPosition = function () {
        const vm = this._view;
        const width = vm.x - vm.base;
        return {
          x: vm.base + width / 2,
          y: vm.y,
        };
      };

      box.pivot();
    },

    // From controller.bar
    getRuler: function (index) {
      const me = this;
      const meta = me.getMeta();
      const yScale = me.getScaleForId(meta.yAxisID);
      const datasetCount = me.getBarCount();

      let tickHeight;
      if (yScale.options.type === 'category') {
        tickHeight = yScale.getPixelForTick(index + 1) - yScale.getPixelForTick(index);
      } else {
        // Average width
        tickHeight = yScale.width / yScale.ticks.length;
      }
      const categoryHeight = tickHeight * yScale.options.categoryPercentage;
      const categorySpacing = (tickHeight - (tickHeight * yScale.options.categoryPercentage)) / 2;
      let fullBarHeight = categoryHeight / datasetCount;

      if (yScale.ticks.length !== me.chart.data.labels.length) {
        const perc = yScale.ticks.length / me.chart.data.labels.length;
        fullBarHeight = fullBarHeight * perc;
      }

      const barHeight = fullBarHeight * yScale.options.barPercentage;
      const barSpacing = fullBarHeight - (fullBarHeight * yScale.options.barPercentage);

      return {
        datasetCount: datasetCount,
        tickHeight: tickHeight,
        categoryHeight: categoryHeight,
        categorySpacing: categorySpacing,
        fullBarHeight: fullBarHeight,
        barHeight: barHeight,
        barSpacing: barSpacing
      };
    },

    // From controller.bar
    getBarCount: function () {
      const me = this;
      let barCount = 0;
      helpers.each(me.chart.data.datasets, function (dataset, datasetIndex) {
        const meta = me.chart.getDatasetMeta(datasetIndex);
        if (meta.bar && me.chart.isDatasetVisible(datasetIndex)) {
          ++barCount;
        }
      }, me);
      return barCount;
    },

    // From controller.bar
    calculateBarHeight: function (ruler) {
      const me = this;
      const yScale = me.getScaleForId(me.getMeta().yAxisID);
      if (yScale.options.barThickness) {
        return yScale.options.barThickness;
      }
      return yScale.options.stacked ? ruler.categoryHeight : ruler.barHeight;
    },

    draw: function (easingValue) {
      const meta = this.getMeta();
      const chartArea = this.chart.chartArea;
      const yScale = this.getScaleForId(meta.yAxisID);
      const xScale = this.getScaleForId(meta.xAxisID);
      const context = this.chart.ctx;
      const hTickLinesY = (yScale.ticks.map((val) => yScale.getPixelForValue(val)));
      // draw the chart
      Chart.DatasetController.prototype.draw.apply(this, arguments);
      // draw the tick
      const tickNum = 4;
      const normalHeight = 8;
      const middleHeight = 16;
      const tickSpace = xScale.getPixelForTick(1) - xScale.getPixelForTick(0);
      const subTickSpace = tickSpace / tickNum;
      const subTickToDraws = [];
      const xScaleTickToDraw = xScale.ticksAsNumbers.slice(0, xScale.ticks.length - 1);
      const hLinesY = [...hTickLinesY.slice(1), chartArea.bottom];
      for (const tickLineY of hLinesY) {
        let xTickIndex = 0;
        for (const tick of xScaleTickToDraw) {
          for (let i = 1; i < tickNum; i++) {
            let lineWidth = helpers.getValueAtIndexOrDefault(
              xScale.options.gridLines.lineWidth,
              xTickIndex)
            ;
            let lineColor = helpers.getValueAtIndexOrDefault(xScale.options.gridLines.color, xTickIndex);
            const xScaleTickX = xScale.getPixelForValue(tick); // xvalues for grid lines
            subTickToDraws.push({
              x: xScaleTickX + i * subTickSpace,
              y: tickLineY,
              dy: (i === 2 ? middleHeight : normalHeight),
              lineWidth: lineWidth,
              lineColor: lineColor
            });
          }
          xTickIndex++;
        }
      }
      for (const itemToDraw of subTickToDraws) {
        if (xScale.options.gridLines.display) {
          context.save();
          context.lineWidth = itemToDraw.lineWidth;
          context.strokeStyle = itemToDraw.lineColor;
          context.beginPath();
          context.moveTo(itemToDraw.x, itemToDraw.y);
          context.lineTo(itemToDraw.x, itemToDraw.y - itemToDraw.dy);
          context.stroke();
          context.restore();
        }
      }
      // draw call out
      for (let element of meta.data) {
        element.drawCallOut();
      }
      // draw DST
      const dst = this.chart.options.getDST();
      // the diameter of the bubble should be min of distance between 1 hour and bar height
      const dstDia = Math.min(xScale.getPixelForValue(1) - xScale.getPixelForValue(0), barHeight);
      if (dst) {
        this.drawDST(
          context,
          dst.value,
          xScale.getPixelForValue(dst.xValue),
          yScale.getPixelForValue(dst.yValue) + YPADDING / 2 + barHeight - dstDia,
          dstDia,
        );
      }
    },

    drawCallOutHelper(context, text, x, y, options, sharpCorners) {
      context.save();
      context.font = helpers
        .fontString(options.fontSize, globalDefaults.defaultFontStyle, globalDefaults.defaultFontFamily);
      context.textBaseline = 'top';
      context.fillStyle = options.fill;
      const padding = options.padding;
      const textSize = context.measureText(text);
      const boxHeight = options.fontSize + padding;
      const boxWidth = textSize.width + padding;
      // make sure pointer corner is centered vertically
      y -= (sharpCorners.bottomRight || sharpCorners.bottomLeft ? options.fontSize : 0);
      this.drawCallOutBoxHelper(context, x - padding / 2, y - padding / 2, boxWidth, boxHeight, sharpCorners);
      context.fill();
      context.fillStyle = '#f0f0f0';
      context.fillText(text, x, y);
      context.restore();
    },

    drawCallOutBoxHelper(context, x, y, width, height, sharpCorners, radius = 10) {
      sharpCorners = sharpCorners || {};
      context.beginPath();
      context.moveTo(x + (sharpCorners.topLeft ? 0 : radius), y);
      context.lineTo(x + width - (sharpCorners.topRight ? 0 : radius), y);
      if (!sharpCorners.topRight) {
        context.quadraticCurveTo(x + width, y, x + width, y + radius);
      }
      context.lineTo(x + width, y + height - (sharpCorners.bottomRight ? 0 : radius));
      if (!sharpCorners.bottomRight) {
        context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
      }
      context.lineTo(x + (sharpCorners.bottomLeft ? 0 : radius), y + height);
      if (!sharpCorners.bottomLeft) {
        context.quadraticCurveTo(x, y + height, x, y + height - radius);
      }
      context.lineTo(x, y + (sharpCorners.topLeft ? 0 : radius));
      if (!sharpCorners.topLeft) {
        context.quadraticCurveTo(x, y, x + radius, y);
      }
      context.closePath();
    },

    drawDST(context, isDST, x, y, diameter = 20) {
      context.save();
      const radius = diameter / 2;
      const center = { x: x + radius, y: y + radius };
      const text = (isDST ? DST_MINUS_TEXT : DST_PLUS_TEXT);
      context.beginPath();
      context.fillStyle = DST_BG;
      context.strokeStyle = DST_BG;
      context.lineWidth = 1;
      context.arc(center.x, center.y, radius, 0, 2 * Math.PI, false);
      context.stroke();
      context.fill();
      // text
      context.textBaseline = 'top';
      context.font = helpers
        .fontString(radius, globalDefaults.defaultFontStyle, globalDefaults.defaultFontFamily);
      context.fillStyle = DST_TEXT_COLOR;
      // -1 is just random adjustment
      context.fillText(text, center.x - radius / 2, center.y - 1 - radius / 2);
      context.restore();
    },

    setHoverStyle: function (e) {
      // TODO: Implement this
    },

    removeHoverStyle: function (e) {
      // TODO
    }

  });

  Chart.defaults.timeline = {

    hover: {
      mode: 'single'
    },

    colorFunction: function (x) {
      return 'black';
    },

    borderColor: '#2b2c2d',
    borderWidth: 0.5,

    callOut: {
      fontSize: 12,
      fill: '#283339',
      padding: 10
    },

    layout: {
      padding: {
        left: 5,
        right: 5,
        top: 0
      }
    },

    legend: false,

    scales: {
      xAxes: [{
        type: 'daily',
        position: 'bottom',
        gridLines: {
          display: true,
          offsetGridLines: true,
          drawBorder: true,
          drawTicks: true
        }
      }],
      yAxes: [
        {
          type: 'category',
          position: 'left',
          stacked: 'true',
          ticks: {
            padding: 10
          },
          gridLines: {
            tickMarkLength: 0,
            display: true,
            offsetGridLines: true,
            drawBorder: true,
            drawTicks: true
          },
        },
        {
          type: 'categoryTotal',
          position: 'right',
          stacked: 'true',
          ticks: {
            padding: 20,
            callback: function (value, index, values) {
              return '00:00:00';
            },
            totalCallback: function () {
              return '00:00:00';
            }
          },
          gridLines: {
            display: true,
            tickMarkLength: 0,
            offsetGridLines: true,
            drawBorder: true,
            drawTicks: true
          }
        },
      ]
    },
    tooltips: {
      enabled: false,
      mode: 'nearest',
      intersect: true,
      position: 'nearest',
      callbacks: {
        title: function (tooltipItems, data) {
          return data.labels[tooltipItems[0].datasetIndex];
        },
        label: function (tooltipItem, data) {
          return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
        }
      }
    }
  };
}
