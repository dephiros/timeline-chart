import * as moment from 'moment-timezone';
import 'moment-duration-format';
import {registerTimeline} from "./timeline";
import {registerDailyScale} from "./daily.scale";
import {registerCategoryTotalScale} from "./category-total.scale";
import {Chart} from 'chart.js';

import "./index.css";
import { HosGridComponent } from "./hos-grid.component";

const TIMEZONES = ['US/Hawaii', 'America/Los_Angeles', 'America/Los_Angeles', 'America/Los_Angeles', 'America/Los_Angeles'];
const DUTY_STATUS_TOTAL = [
  {
    "seconds": 1837,
    "event_code": 3
  },
  {
    "seconds": 5400,
    "event_code": 4
  },
  {
    "seconds": 11557,
    "event_code": 2
  },
  {
    "seconds": 5400,
    "event_code": 1
  }
];

const DUTY_STATUSES = [
  [{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503914400,"event_code_name":"SB","end_epoch":1503921467,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503921467,"event_code_name":"DRIVING","end_epoch":1503923267,"violations":[{"ruleset_id":1,"code":"DD"},{"ruleset_id":1,"code":"DOD"},{"ruleset_id":1,"code":"DR"},{"ruleset_id":1,"code":"7D"}]},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503923267,"event_code_name":"OFF_DUTY","end_epoch":1503925067,"violations":null},{"sds":{"event_type_name":"SDS","event_code_name":"PC"},"event_type_name":"DUTY_STATUS","start_epoch":1503925067,"event_code_name":"OFF_DUTY","end_epoch":1503926867,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503926867,"event_code_name":"OFF_DUTY","end_epoch":1503928667,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503928667,"event_code_name":"ON_DUTY","end_epoch":1503930467,"violations":null},{"sds":{"event_type_name":"SDS","event_code_name":"YM"},"event_type_name":"DUTY_STATUS","start_epoch":1503930467,"event_code_name":"ON_DUTY","end_epoch":1503932267,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503932267,"event_code_name":"ON_DUTY","end_epoch":1503934067,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503934067,"event_code_name":"SB","end_epoch":1503934068,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503934068,"event_code_name":"DRIVING","end_epoch":1503934105,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1503934105,"event_code_name":"SB","is_current_status":true,"end_epoch":1503937709,"violations":null}],

  [{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509865200,"event_code_name":"OFF_DUTY","end_epoch":1509868980,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509868980,"event_code_name":"DRIVING","end_epoch":1509870000,"violations":[{"ruleset_id":1,"code":"DD"},{"ruleset_id":1,"code":"DOD"},{"ruleset_id":1,"code":"DR"},{"ruleset_id":1,"code":"7D"}]},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509870000,"event_code_name":"SB","end_epoch":1509870900,"violations":null},{"event_type_name":"DUTY_STATUS","start_epoch":1509870900,"event_code_name":"ON_DUTY","end_epoch":1509878400,"violations":null}],

  [{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509865200,"event_code_name":"OFF_DUTY","end_epoch":1509872580,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509872580,"event_code_name":"DRIVING","end_epoch":1509873600,"violations":[{"ruleset_id":1,"code":"DD"},{"ruleset_id":1,"code":"DOD"},{"ruleset_id":1,"code":"DR"},{"ruleset_id":1,"code":"7D"}]},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509873600,"event_code_name":"SB","end_epoch":1509874500,"violations":null},{"event_type_name":"DUTY_STATUS","start_epoch":1509874500,"event_code_name":"ON_DUTY","end_epoch":1509878400,"violations":null}],

  [{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509865200,"event_code_name":"OFF_DUTY","end_epoch":1509868980,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509868980,"event_code_name":"DRIVING","end_epoch":1509870000,"violations":[{"ruleset_id":1,"code":"DD"},{"ruleset_id":1,"code":"DOD"},{"ruleset_id":1,"code":"DR"},{"ruleset_id":1,"code":"7D"}]},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509870000,"event_code_name":"SB","end_epoch":1509870900,"violations":null},{"sds":{"event_type_name":"SDS","event_code_name":"PC"},"event_type_name":"DUTY_STATUS","start_epoch":1509870900,"event_code_name":"ON_DUTY","end_epoch":1509872700,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509872700,"event_code_name":"SB","end_epoch":1509873900,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509873900,"event_code_name":"DRIVING","end_epoch":1509874800,"violations":null},{"sds":{"event_type_name":"SDS","event_code_name":"YM"},"event_type_name":"DUTY_STATUS","start_epoch":1509874800,"event_code_name":"OFF_DUTY","end_epoch":1509876900,"violations":null}],

  [{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509865200,"event_code_name":"OFF_DUTY","end_epoch":1509868980,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509868980,"event_code_name":"DRIVING","end_epoch":1509870000,"violations":[{"ruleset_id":1,"code":"DD"},{"ruleset_id":1,"code":"DOD"},{"ruleset_id":1,"code":"DR"},{"ruleset_id":1,"code":"7D"}]},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509870000,"event_code_name":"SB","end_epoch":1509870900,"violations":null},{"sds":{"event_type_name":"SDS","event_code_name":"PC"},"event_type_name":"DUTY_STATUS","start_epoch":1509870900,"event_code_name":"ON_DUTY","end_epoch":1509872700,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509872700,"event_code_name":"DRIVING","end_epoch":1509873300,"violations":null},{"sds":null,"event_type_name":"DUTY_STATUS","start_epoch":1509873300,"event_code_name":"SB","end_epoch":1509874800,"violations":null},{"sds":{"event_type_name":"SDS","event_code_name":"YM"},"event_type_name":"DUTY_STATUS","start_epoch":1509874800,"event_code_name":"OFF_DUTY","end_epoch":1509876900,"violations":null}],
];

export function app(): void {
  registerTimeline();
  registerDailyScale();
  registerCategoryTotalScale();

  const dsLen = DUTY_STATUSES.length;

  for (let i = 0; i < dsLen; i++) {
    new HosGridComponent(
      document.querySelector('.chart-container') as HTMLCanvasElement,
      DUTY_STATUSES[i],
      DUTY_STATUS_TOTAL,
      TIMEZONES[i]
    );
  }
}

window.addEventListener('load', app);
