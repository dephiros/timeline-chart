"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function registerScale() {
    var helpers = Chart.helpers;
    var isArray = helpers.isArray;
    var DailyScale = Chart.scaleService.getScaleConstructor('linear').extend({});
    Chart.scaleService.registerScaleType('daily', DailyScale);
}
exports.registerScale = registerScale;
