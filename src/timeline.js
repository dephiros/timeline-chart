"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function registerTimeline() {
    var helpers = Chart.helpers;
    var isArray = helpers.isArray;
    Chart.controllers.timeline = Chart.DatasetController.extend({
        dataElementType: Chart.elements.Rectangle,
        update: function (reset) {
            var me = this;
            var meta = me.getMeta();
            var dataset = me.getDataset();
            helpers.each(meta.data, function (box, index) {
                me.updateElement(box, index, reset);
            }, me);
        },
        updateElement: function (box, index, reset) {
            var me = this;
            var meta = me.getMeta();
            var xScale = me.getScaleForId(meta.xAxisID);
            var yScale = me.getScaleForId(meta.yAxisID);
            var dataset = me.getDataset();
            var data = dataset.data[index];
            var datasetIndex = me.index;
            box._xScale = xScale;
            box._yScale = yScale;
            box._datasetIndex = me.index;
            box._index = index;
            var ruler = me.getRuler(index);
            var x = xScale.getPixelForValue(data.start, index, datasetIndex);
            var end = xScale.getPixelForValue(data.end, index, datasetIndex);
            var y = yScale.getPixelForValue(data, datasetIndex, datasetIndex);
            var width = end - x;
            var height = ruler.tickHeight;
            var color = me.chart.options.colorFunction(data);
            box._model = {
                x: x,
                y: y,
                width: width,
                height: height,
                base: y + height,
                backgroundColor: color,
                // Tooltip
                label: me.chart.data.labels[index],
                datasetLabel: dataset.label
            };
            box.draw = function () {
                var ctx = this._chart.ctx;
                var vm = this._view;
                ctx.fillStyle = vm.backgroundColor;
                helpers.drawRoundedRectangle(ctx, vm.x, vm.y, vm.width, vm.height, 2);
                ctx.fill();
            };
            box.tooltipPosition = function () {
                var vm = this._view;
                return {
                    x: vm.x + vm.width / 2,
                    y: vm.y + vm.height / 2,
                };
            };
            box.pivot();
        },
        // From controller.bar
        getRuler: function (index) {
            var me = this;
            var meta = me.getMeta();
            var yScale = me.getScaleForId(meta.yAxisID);
            var datasetCount = me.getBarCount();
            var tickHeight;
            if (yScale.options.type === 'category') {
                tickHeight = yScale.getPixelForTick(index + 1) - yScale.getPixelForTick(index);
            }
            else {
                // Average width
                tickHeight = yScale.width / yScale.ticks.length;
            }
            var categoryHeight = tickHeight * yScale.options.categoryPercentage;
            var categorySpacing = (tickHeight - (tickHeight * yScale.options.categoryPercentage)) / 2;
            var fullBarHeight = categoryHeight / datasetCount;
            if (yScale.ticks.length !== me.chart.data.labels.length) {
                var perc = yScale.ticks.length / me.chart.data.labels.length;
                fullBarHeight = fullBarHeight * perc;
            }
            var barHeight = fullBarHeight * yScale.options.barPercentage;
            var barSpacing = fullBarHeight - (fullBarHeight * yScale.options.barPercentage);
            return {
                datasetCount: datasetCount,
                tickHeight: tickHeight,
                categoryHeight: categoryHeight,
                categorySpacing: categorySpacing,
                fullBarHeight: fullBarHeight,
                barHeight: barHeight,
                barSpacing: barSpacing
            };
        },
        // From controller.bar
        getBarCount: function () {
            var me = this;
            var barCount = 0;
            helpers.each(me.chart.data.datasets, function (dataset, datasetIndex) {
                var meta = me.chart.getDatasetMeta(datasetIndex);
                if (meta.bar && me.chart.isDatasetVisible(datasetIndex)) {
                    ++barCount;
                }
            }, me);
            return barCount;
        },
        // From controller.bar
        calculateBarHeight: function (ruler) {
            var me = this;
            var yScale = me.getScaleForId(me.getMeta().yAxisID);
            if (yScale.options.barThickness) {
                return yScale.options.barThickness;
            }
            return yScale.options.stacked ? ruler.categoryHeight : ruler.barHeight;
        },
        setHoverStyle: function (e) {
            // TODO: Implement this
        },
        removeHoverStyle: function (e) {
            // TODO
        }
    });
    Chart.defaults.timeline = {
        hover: {
            mode: 'single'
        },
        colorFunction: function (x) {
            return 'black';
        },
        layout: {
            padding: {
                left: 5,
                right: 5,
                top: 0
            }
        },
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                    type: 'daily',
                    ticks: {
                        min: 0,
                        max: 10
                    },
                    position: 'bottom',
                    gridLines: {
                        display: true,
                        offsetGridLines: true,
                        drawBorder: true,
                        drawTicks: true
                    }
                }],
            yAxes: [{
                    type: 'category',
                    position: 'left',
                    stacked: 'true',
                    gridLines: {
                        display: true,
                        offsetGridLines: true,
                        drawBorder: true,
                        drawTicks: true
                    }
                }]
        },
        tooltips: {
            callbacks: {
                title: function (tooltipItems, data) {
                    return data.labels[tooltipItems[0].datasetIndex];
                },
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                }
            }
        }
    };
}
exports.registerTimeline = registerTimeline;
