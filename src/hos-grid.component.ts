import * as moment from 'moment-timezone';
import 'moment-duration-format';
import {Chart} from 'chart.js';

export class HosGridComponent {
  public static CONTAINER_CLASS = 'canvas-container';
  public static DEFAULT_TZ = "America/Los_Angeles";
  public static SDS_COLORS = {OK: '#4ACF49', V: '#EE4C38', SDS: '#5B6770'};
  public static get DEFAULT_GROUPED_DS () {
    return {OFF_DUTY: [], ON_DUTY: [], DRIVING: [], SB: [], DST: null};
  }
  public static EVENT_NAME_TO_ABBR = {
    OFF_DUTY: 'OFF',
    ON_DUTY: 'ON',
    DRIVING: 'D',
    SB: 'SB'
  };
  public static EVENT_NAME_TO_CODE = {
    'OFF': 1,
    'SB': 2,
    'D': 3,
    'ON': 4
  };
  private chart;
  private canvas;
  private data;

  private plugins = {
    beforeDraw: function (chartInstance: any, easing) {
      const meta = chartInstance.getDatasetMeta(0);
      const chartArea = chartInstance.chart.chartArea;
      const yScale = chartInstance.scales[meta.yAxisID];
      const context = chartInstance.ctx;
      const hTickLinesY = (yScale.ticks.map((val) => yScale.getPixelForValue(val)));
      // draw background
      const gradient = context.createLinearGradient(0, chartArea.top, 0, chartArea.bottom);
      const gradientColor = ['#FFFFFF', '#E2E4E6'];
      const gradientLines = [...hTickLinesY, chartArea.bottom - 1];
      const getGradientStop = (y) => (y - gradientLines[0]) / (gradientLines[gradientLines.length - 1] - gradientLines[0]);
      let lastColor = "";
      for (let i = 0; i < gradientLines.length; i++) {
        if (lastColor) {
          gradient.addColorStop(getGradientStop(gradientLines[i]), lastColor);
        }
        lastColor = gradientColor[i % gradientColor.length];
        gradient.addColorStop(getGradientStop(gradientLines[i]), lastColor);
      }
      context.save();
      context.clearRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
      context.fillStyle = gradient;
      context.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
      context.restore();
    }
  };

  private options = {
    fillFunction: HosGridComponent.fillFunction,
    getDST: () => this.data.DST,
    tooltips: {
      enabled: true,
      callbacks: {
        label: (tooltipItem, data) => {
          const itemData = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          const dutyStatus = itemData.dutyStatus;
          const duration = moment.duration(dutyStatus.end_epoch - dutyStatus.start_epoch, 's');
          return [
            duration.humanize(),
            moment.unix(dutyStatus.start_epoch).tz(this.timeZone).format('HH:mm:ss z'),
            moment.unix(dutyStatus.end_epoch).tz(this.timeZone).format('HH:mm:ss z')
          ];
        }
      }
    },
    maintainAspectRatio: false,
    height: 300,
    scales: {
      yAxes: [
        {},
        {
          ticks: {
            callback: (value, index, values) => {
              const total = this.dutyStatusTotals
                .find((total) => total.event_code === HosGridComponent.EVENT_NAME_TO_CODE[value]);
              return total ? moment.duration(total.seconds, 's').format('hh:mm:ss', { trim: false }) : "00:00:00";
            },
            totalCallback: (values) => {
              return this.dutyStatusTotals
                .reduce((total, current) => {
                  return total.add((current && current.seconds) || 0, 's');
                }, moment.duration())
                .format('hh:mm:ss', { trim: false });
            }
          }
        }
      ]
    }
  };

  private initCanvas() {
    if (!this.canvas) {
      this.canvas = document.createElement('canvas');
      const container = document.createElement('div');
      container.classList.add(HosGridComponent.CONTAINER_CLASS);
      container.appendChild(this.canvas);
      this.container.appendChild(container);
    }
  }

  private initChart() {
    if (this.chart) {
      this.chart.destroy();
    } else {
      this.data = this.parseDutyStatus(this.dutyStatuses, HosGridComponent.getAverageEpochDS, this.timeZone);
      this.chart = new Chart(this.canvas.getContext('2d'), {
        type: 'timeline',
        options: this.options,
        data: {
          labels: ["OFF", "SB", "D", 'ON'],
          datasets: [{
            data: this.data.OFF_DUTY
          }, {
            data: this.data.SB
          }, {
            data: this.data.DRIVING
          }, {
            data: this.data.ON_DUTY
          }]
        },
      })
    }
  }

  constructor(private container: HTMLElement,
              private dutyStatuses,
              private dutyStatusTotals,
              private timeZone = HosGridComponent.DEFAULT_TZ
  ) {
    this.initCanvas();
    this.initChart();
  }

  private parseDutyStatus(dutyStatuses, getDate, timezone = HosGridComponent.DEFAULT_TZ) {
    let groupedDS = HosGridComponent.DEFAULT_GROUPED_DS;
    if (!dutyStatuses) { return groupedDS; }
    const currentDate = moment.unix(getDate(dutyStatuses)).tz(timezone).startOf('day');
    for (let dutyStatus of  dutyStatuses) {
      groupedDS[dutyStatus.event_code_name] = groupedDS[dutyStatus.event_code_name] || [];
      const start = HosGridComponent.getHoursBetweenWithTZ(
        moment.unix(dutyStatus.start_epoch).tz(timezone),
        currentDate
      );
      const end = HosGridComponent.getHoursBetweenWithTZ(
        moment.unix(dutyStatus.end_epoch).tz(timezone),
        currentDate
      );
      groupedDS[dutyStatus.event_code_name].push({
        start: start.hours,
        end: end.hours,
        dutyStatus: dutyStatus,
        callOut: HosGridComponent.getSDSCallout(dutyStatus),
        type: HosGridComponent.getEventType(dutyStatus)
      });
      // if DSTVal not 0 then we have a transition
      const DSTVal = start.DSTVal || end.DSTVal;
      // we want the first event that cross DST
      if (DSTVal && !groupedDS.DST) {
        groupedDS.DST = {
          yValue: this.getEventName(dutyStatus.event_code_name),
          xValue: DSTVal > 0 ? 2 : 1, // if fallback then draw at 2 else 1
          val: DSTVal
        };
      }
    }
    return groupedDS;
  }

  private getEventName(codeName) {
    return HosGridComponent.EVENT_NAME_TO_ABBR[codeName];
  }

  private static getHoursBetweenWithTZ(momentObj: moment.Moment, momentToday: moment.Moment) {
    // ex 02:00 PST - 00:00 PDT = 2 hours but shown as 1 hour on the chart
    const utcOffsetDiff = (momentObj.utcOffset() - momentToday.utcOffset()) / 60;
    return {
      hours: momentObj.diff(momentToday, 'hour', true) + utcOffsetDiff,
      DSTVal: -utcOffsetDiff
    };
  }

  private static getSDSCallout(dutyStatus) {
    return (dutyStatus.sds && dutyStatus.sds.event_code_name) || "";
  }

  private static getEventType(dutyStatus) {
    if (dutyStatus.violations && dutyStatus.violations.length) {
      return 'V';
    } else if (dutyStatus.sds) {
      return 'SDS';
    }
    return 'OK';
  }

  private static getAverageEpochDS(dutyStatuses) {
    dutyStatuses = dutyStatuses || [];
    return Math.round(((dutyStatuses)
      .reduce((sum, dutyStatus) => {
        // console.log(sum, dutyStatus.start_epoch);
        return dutyStatus.start_epoch + sum;
      }, 0)) / (dutyStatuses.length || 1));
  }

  private static fillFunction(val) {
    return HosGridComponent.SDS_COLORS[val.type || 'OK'];
  }

}
