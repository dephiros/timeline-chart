// `CheckerPlugin` is optional. Use it if you want async error reporting.
// We need this plugin to detect a `--watch` mode. It may be removed later
// after https://github.com/webpack/webpack/issues/3460 will be resolved.
const { CheckerPlugin } = require('awesome-typescript-loader');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const root = path.resolve('.');

module.exports = {
  context: path.resolve(root, "src"),
  entry: './main.ts',
  // Currently we need to add '.ts' to the resolve.extensions array.
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  output: {
    path: path.resolve(root, 'dist'),
    publicPath: "/",
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].map'
  },
  // Source maps support ('inline-source-map' also works)
  devtool: 'cheap-module-source-map',

  // Add the loader for .ts files.
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      }
    ]
  },
  plugins: [
    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      title: 'Timeline',
      template: 'index.ejs'
    }),
    new webpack.EnvironmentPlugin([
      'NODE_ENV'
    ])
  ]
};
