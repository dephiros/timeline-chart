const merge = require('webpack-merge');
const baseConfig = require('./webpack.common');
const path = require('path');
const root = path.resolve('.');

module.exports = merge(baseConfig, {
  devServer: {
    contentBase: path.resolve(root, "dist"),
    compress: true,
    port: 9000,
    open: true
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader?importLoaders=1'
        ]
      }
    ]
  }
});
